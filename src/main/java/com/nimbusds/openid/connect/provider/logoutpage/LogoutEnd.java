package com.nimbusds.openid.connect.provider.logoutpage;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import net.minidev.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;


/**
 * Logout end.
 *
 * See https://connect2id.com/products/server/docs/integration/logout-session#logout-end
 */
public final class LogoutEnd extends LogoutAPIResponseMessage {
	
	
	/**
	 * The post logout redirection URI, {@code null} if not specified.
	 */
	private final URI postLogoutRedirectURI;
	
	
	/**
	 * The front-channel logout URIs, {@code null} if not specified.
	 */
	private final List<URI> frontChannelLogoutURIs;
	
	
	/**
	 * Creates a new logout end.
	 *
	 * @param postLogoutRedirectURI  The post logout redirection URI,
	 *                               {@code null} if not specified.
	 * @param frontChannelLogoutURIs The front-channel logout URIs,
	 *                               {@code null} if not specified.
	 */
	public LogoutEnd(final URI postLogoutRedirectURI,
			 final List<URI> frontChannelLogoutURIs) {
		this.postLogoutRedirectURI = postLogoutRedirectURI;
		this.frontChannelLogoutURIs = frontChannelLogoutURIs;
	}
	
	
	/**
	 * Returns the post logout redirection URI.
	 *
	 * @return The post logout redirection URI, {@code null} if not
	 *         specified.
	 */
	public URI getPostLogoutRedirectionURI() {
		return postLogoutRedirectURI;
	}
	
	
	/**
	 * Returns the front-channel logout URIs.
	 *
	 * @return The front-channel logout URIs, {@code null} if not
	 *         specified.
	 */
	public List<URI> getFrontChannelLogoutURIs() {
		return frontChannelLogoutURIs;
	}
	
	
	/**
	 * Parses a logout end from the specified JSON object.
	 *
	 * @param jsonObject The JSON object.
	 *
	 * @return The logout end.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static LogoutEnd parse(final JSONObject jsonObject)
		throws ParseException {
		
		if (!"end".equals(JSONObjectUtils.getString(jsonObject, "type"))) {
			throw new ParseException("The JSON object is not a logout end message");
		}
		
		URI postLogoutRedirectURI = null;
		
		if (jsonObject.containsKey("post_logout_redirect_uri")) {
			postLogoutRedirectURI = JSONObjectUtils.getURI(jsonObject, "post_logout_redirect_uri");
		}
		
		List<URI> frontChannelLogoutURIs = null;
		
		if (jsonObject.containsKey("frontchannel_logout_uris")) {
			List<String> values = JSONObjectUtils.getStringList(jsonObject, "frontchannel_logout_uris");
			frontChannelLogoutURIs = new LinkedList<>();
			for (String v: values) {
				try {
					frontChannelLogoutURIs.add(new URI(v));
				} catch (URISyntaxException e) {
					throw new ParseException("Invalid URI: " + v);
				}
			}
		}
		
		return new LogoutEnd(postLogoutRedirectURI, frontChannelLogoutURIs);
	}
}
