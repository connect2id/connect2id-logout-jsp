package com.nimbusds.openid.connect.provider.logoutpage;


import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;


/**
 * Cookie removal routine.
 */
public class CookieRemoval {
	
	
	/**
	 * Removes a cookie using the specified HTTP servlet response.
	 *
	 * @param cookieName      The cookie name.
	 * @param servletResponse The HTTP servlet response to use.
	 */
	public static void removeCookie(final String cookieName,
					final HttpServletResponse servletResponse) {
		
		var cookie = new Cookie(cookieName, "");
		cookie.setMaxAge(0);
		cookie.setPath("/");
		servletResponse.addCookie(cookie);
	}
}
