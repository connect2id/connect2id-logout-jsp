package com.nimbusds.openid.connect.provider.logoutpage;


import com.thetransactioncompany.util.PropertyParseException;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * Logout session API client launcher.
 */
@WebListener
public class LogoutAPIClientLauncher implements ServletContextListener {
	
	
	/**
	 * The logout page configuration file.
	 */
	private static final String CONFIG_FILE = "/WEB-INF/logoutPage.properties";
	
	
	/**
	 * Called on web app startup.
	 *
	 * @param event The event.
	 */
	public void contextInitialized(final ServletContextEvent event) {
		
		LogoutAPIClient logoutAPIClient;
		
		try {
			var props = new Properties();
			InputStream is = event.getServletContext().getResourceAsStream(CONFIG_FILE);
			if (is == null) {
				throw new RuntimeException("Couldn't find logout page properties file " + CONFIG_FILE);
			}
			props.load(is);
			is.close();
			logoutAPIClient = new LogoutAPIClient(props);
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load logout page properties: " + e.getMessage());
		} catch (PropertyParseException e) {
			throw new RuntimeException("Couldn't parse logout page configuration " + e.getPropertyKey() + ": " + e.getMessage());
		}
		
		event.getServletContext().setAttribute(LogoutAPIClient.SERVLET_CTX_NAME, logoutAPIClient);
	}
	
	
	/**
	 * Called on web app shutdown.
	 *
	 * @param event The event.
	 */
	public void contextDestroyed(final ServletContextEvent event) {
	
	}
}
