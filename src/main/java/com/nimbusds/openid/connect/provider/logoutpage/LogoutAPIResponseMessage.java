package com.nimbusds.openid.connect.provider.logoutpage;


/**
 * Logout session API response message.
 */
public abstract class LogoutAPIResponseMessage { }
