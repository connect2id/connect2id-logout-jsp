package com.nimbusds.openid.connect.provider.logoutpage;


import net.minidev.json.JSONObject;

import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;


/**
 * Logout error.
 *
 * See https://connect2id.com/products/server/docs/integration/logout-session#logout-error
 */
public final class LogoutError extends LogoutAPIResponseMessage {
	
	
	/**
	 * The underlying error object.
	 */
	private final ErrorObject errorObject;
	
	
	/**
	 * Creates a new logout error.
	 *
	 * @param errorObject The error object.
	 */
	public LogoutError(final ErrorObject errorObject) {
		this.errorObject = errorObject;
	}
	
	
	/**
	 * Returns the underlying error object.
	 *
	 * @return The error object.
	 */
	public ErrorObject getErrorObject() {
		return errorObject;
	}
	
	
	/**
	 * Parses a logout error from the specified JSON object.
	 *
	 * @param jsonObject The JSON object.
	 *
	 * @return The logout error.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static LogoutError parse(final JSONObject jsonObject)
		throws ParseException {
		
		if (!"error".equals(JSONObjectUtils.getString(jsonObject, "type"))) {
			throw new ParseException("The JSON object is not an error message");
		}
		
		return new LogoutError(ErrorObject.parse(jsonObject));
	}
}
