package com.nimbusds.openid.connect.provider.logoutpage;


import com.nimbusds.common.contenttype.ContentType;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.util.URLUtils;
import com.thetransactioncompany.util.PropertyParseException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import net.minidev.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;


/**
 * Client to the logout session API.
 *
 * See https://connect2id.com/products/server/docs/integration/logout-session
 */
public class LogoutAPIClient {
	
	
	/**
	 * The name of the servlet context parameter for the logout API client.
	 */
	public static final String SERVLET_CTX_NAME = "LogoutAPIClient";
	
	
	/**
	 * The underlying configuration.
	 */
	private final Configuration config;
	
	
	/**
	 * Creates a new logout session API client.
	 *
	 * @param props The configuration properties.
	 *
	 * @throws PropertyParseException If property parsing failed.
	 */
	public LogoutAPIClient(final Properties props)
		throws PropertyParseException {
		
		this(new Configuration(props));
	}
	
	
	/**
	 * Creates a new logout session API client.
	 *
	 * @param config The configuration.
	 */
	public LogoutAPIClient(final Configuration config) {
		this.config = config;
	}
	
	
	/**
	 * Returns the configuration.
	 *
	 * @return The configuration.
	 */
	public Configuration getConfiguration() {
		return config;
	}
	
	
	/**
	 * Returns the subject SID value if a cookie is found.
	 *
	 * @param servletRequest The HTTP servlet request.
	 *
	 * @return The subject SID, {@code null} if no cookie is found.
	 */
	private String getSubjectSID(final HttpServletRequest servletRequest) {
		
		Cookie[] cookies = servletRequest.getCookies();
		
		if (cookies == null) {
			return null;
		}
		
		for (Cookie cookie: servletRequest.getCookies()) {
			if (config.getCookieName().equalsIgnoreCase(cookie.getName())) {
				return cookie.getValue();
			}
		}
		
		return null;
	}
	
	
	/**
	 * Posts a new request to the logout session API.
	 *
	 * See https://connect2id.com/products/server/docs/integration/logout-session#logout-sessions-post
	 *
	 * @param servletRequest The received servlet request at the logout
	 *                       page.
	 *
	 * @return A logout prompt or logout error message.
	 *
	 * @throws IOException    If the web API request failed.
	 * @throws ParseException If parsing the web API response failed.
	 */
	public LogoutAPIResponseMessage postRequest(final HttpServletRequest servletRequest)
		throws IOException, ParseException {

		String subjectSID = getSubjectSID(servletRequest);

		String queryString;
		if ("GET".equalsIgnoreCase(servletRequest.getMethod())) {
			queryString = servletRequest.getQueryString();
		} else if ("POST".equalsIgnoreCase(servletRequest.getMethod())) {
			Map<String, String[]> parameterMapIn = servletRequest.getParameterMap();
			Map<String, List<String>> parameterMapOut = new HashMap<>();
			for (Map.Entry<String, String[]> en: parameterMapIn.entrySet()) {
				parameterMapOut.put(en.getKey(), Arrays.asList(en.getValue()));
			}
			queryString = URLUtils.serializeParameters(parameterMapOut);
		} else {
			throw new ParseException("Unsupported HTTP method");
		}

		return postRequest(subjectSID, queryString);
	}
	
	
	/**
	 * Posts a new request to the logout session API.
	 *
	 * See https://connect2id.com/products/server/docs/integration/logout-session#logout-sessions-post
	 *
	 * @param subjectSID The subject SID, {@code null} if not found.
	 * @param query      The query string, {@code null} if none.
	 *
	 * @return A logout prompt or logout error message.
	 *
	 * @throws IOException    If the web API request failed.
	 * @throws ParseException If parsing the web API response failed.
	 */
	public LogoutAPIResponseMessage postRequest(final String subjectSID, final String query)
		throws IOException, ParseException {
		
		var requestJSONObject = new JSONObject();
		requestJSONObject.put("sub_sid", subjectSID);
		requestJSONObject.put("query", query);
		
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, config.getAPIEndpoint().toURL());
		httpRequest.setAuthorization(config.getAPIAccessToken().toAuthorizationHeader());
		httpRequest.setEntityContentType(ContentType.APPLICATION_JSON);
		httpRequest.setBody(requestJSONObject.toJSONString());
		
		HTTPResponse httpResponse = httpRequest.send();
		
		if (200 != httpResponse.getStatusCode()) {
			throw new IOException("HTTP request to " + config.getAPIEndpoint() + " failed: " + httpResponse.getStatusCode());
		}
		
		JSONObject responseJSONObject = httpResponse.getBodyAsJSONObject();
		
		if ("confirm".equals(responseJSONObject.get("type"))) {
			return LogoutPrompt.parse(responseJSONObject);
		}
		
		if ("error".equals(responseJSONObject.get("type"))) {
			return LogoutError.parse(responseJSONObject);
		}
		
		if ("end".equals(responseJSONObject.get("type"))) {
			return LogoutEnd.parse(responseJSONObject);
		}
		
		throw new ParseException("Unexpected message type: " + responseJSONObject.get("type"));
	}
	
	
	/**
	 * Puts a logout confirmation to the logout session API.
	 *
	 * See https://connect2id.com/products/server/docs/integration/logout-session#logout-sessions-sid-put
	 *
	 * @param servletRequest The HTTP servlet request received at the
	 *                       logout page.
	 *
	 * @return The logout end message.
	 *
	 * @throws IOException    If the web API request failed.
	 * @throws ParseException If parsing the web API response failed.
	 */
	public LogoutEnd putConfirmation(final HttpServletRequest servletRequest)
		throws IOException, ParseException {
		
		String logoutSID = servletRequest.getParameter("logout_sid");
		
		boolean userConfirmsLogout = "true".equals(servletRequest.getParameter("op_logout"));
		
		return putConfirmation(logoutSID, userConfirmsLogout);
	}
	
	
	/**
	 * Puts a logout confirmation to the logout session API.
	 *
	 * See https://connect2id.com/products/server/docs/integration/logout-session#logout-sessions-sid-put
	 *
	 * @param logoutSID          The logout SID.
	 * @param userConfirmsLogout {@code true} if the user confirmed logout,
	 *                           else {@code false}.
	 *
	 * @return The logout end message.
	 *
	 * @throws IOException    If the web API request failed.
	 * @throws ParseException If parsing the web API response failed.
	 */
	public LogoutEnd putConfirmation(final String logoutSID, final boolean userConfirmsLogout)
		throws IOException, ParseException {
	
		var requestJSONObject = new JSONObject();
		requestJSONObject.put("op_logout", userConfirmsLogout);
		requestJSONObject.put("confirm_logout", userConfirmsLogout); // backward compatibility for c2id 12.14 and older
		
		var httpRequest = new HTTPRequest(HTTPRequest.Method.PUT, composeLogoutSessionURL(logoutSID));
		httpRequest.setAuthorization(config.getAPIAccessToken().toAuthorizationHeader());
		httpRequest.setEntityContentType(ContentType.APPLICATION_JSON);
		httpRequest.setBody(requestJSONObject.toJSONString());
		
		HTTPResponse httpResponse = httpRequest.send();
		
		if (200 != httpResponse.getStatusCode()) {
			throw new IOException("HTTP request to " + config.getAPIEndpoint() + " failed: " + httpResponse.getStatusCode());
		}
		
		JSONObject responseJSONObject = httpResponse.getContentAsJSONObject();
	
		return LogoutEnd.parse(responseJSONObject);
	}
	
	
	/**
	 * Composes a logout session URL.
	 *
	 * @param logoutSID The logout SID.
	 *
	 * @return The logout session URL.
	 *
	 * @throws MalformedURLException If URL composition failed.
	 */
	private URL composeLogoutSessionURL(final String logoutSID)
		throws MalformedURLException {
		
		if (config.getAPIEndpoint().toString().endsWith("/")) {
			return new URL(config.getAPIEndpoint() + logoutSID);
		} else {
			return new URL(config.getAPIEndpoint() + "/" + logoutSID);
		}
	}
}
