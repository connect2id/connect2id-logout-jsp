package com.nimbusds.openid.connect.provider.logoutpage;


import net.minidev.json.JSONObject;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;


/**
 * Logout prompt.
 *
 * See https://connect2id.com/products/server/docs/integration/logout-session#logout-prompt
 */
public final class LogoutPrompt extends LogoutAPIResponseMessage {
	
	
	/**
	 * The logout SID.
	 */
	private final String logoutSID;
	
	
	/**
	 * The current session subject.
	 */
	private final Subject subject;
	
	
	/**
	 * Optional claims included in the subject session.
	 */
	private final JSONObject sessionClaims;
	
	
	/**
	 * Optional data included in the subject session.
	 */
	private final JSONObject sessionData;
	
	
	/**
	 * Creates a new logout prompt.
	 *
	 * @param logoutSID     The logout SID.
	 * @param subject       The current session subject.
	 * @param sessionClaims Optional claims included in the subject
	 *                      session, {@code null} if none.
	 * @param sessionData   Optional data included in the subject session,
	 *                      {@code null} if none.
	 */
	public LogoutPrompt(final String logoutSID,
			    final Subject subject,
			    final JSONObject sessionClaims,
			    final JSONObject sessionData) {
		this.logoutSID = logoutSID;
		this.subject = subject;
		this.sessionClaims = sessionClaims;
		this.sessionData = sessionData;
	}
	
	
	/**
	 * Returns the logout SID.
	 *
	 * @return The logout SID.
	 */
	public String getLogoutSID() {
		return logoutSID;
	}
	
	
	/**
	 * Returns the session subject (end-user).
	 *
	 * @return The session subject.
	 */
	public Subject getSubject() {
		return subject;
	}
	
	
	/**
	 * Returns the optional session claims.
	 *
	 *
	 * @return The session claims, {@code null} if not specified.
	 */
	public JSONObject getSessionClaims() {
		return sessionClaims;
	}
	
	
	/**
	 * Returns the optional session data.
	 *
	 * @return The session data, {@code null} if not specified.
	 */
	public JSONObject getSessionData() {
		return sessionData;
	}
	
	
	/**
	 * Parses a logout prompt from the specified JSON object.
	 *
	 * @param jsonObject The JSON object.
	 *
	 * @return The logout prompt.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static LogoutPrompt parse(final JSONObject jsonObject)
		throws ParseException {
		
		if (!"confirm".equals(JSONObjectUtils.getString(jsonObject, "type"))) {
			throw new ParseException("The JSON object is not a prompt message");
		}
		
		String logoutSID = JSONObjectUtils.getString(jsonObject, "sid");
		
		// Default `true` for Connect2id server prior to 12.14
		boolean idTokenHintPresent = JSONObjectUtils.getBoolean(jsonObject, "id_token_hint_present", true);
		
		JSONObject subjectSession = JSONObjectUtils.getJSONObject(jsonObject, "sub_session");
		var sub = new Subject(JSONObjectUtils.getString(subjectSession, "sub"));
		JSONObject sessionClaims = subjectSession.containsKey("claims") ?
			JSONObjectUtils.getJSONObject(subjectSession, "claims") : null;
		JSONObject sessionData = subjectSession.containsKey("data") ?
			JSONObjectUtils.getJSONObject(subjectSession, "data") : null;
		
		return new LogoutPrompt(logoutSID, sub, sessionClaims, sessionData);
	}
}
