package com.nimbusds.openid.connect.provider.logoutpage;


import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;


/**
 * Logout page configuration.
 */
public final class Configuration {
	
	
	/**
	 * The Connect2id server logout session API endpoint.
	 */
	private final URI apiEndpoint;
	
	
	/**
	 * The bearer access token for the logout session API.
	 */
	private final BearerAccessToken apiAccessToken;
	
	
	/**
	 * The name of the cookie used to store the subject (end-user) session
	 * ID.
	 */
	private final String cookieName;
	
	
	/**
	 * Optional default post logout redirection URI.
	 */
	private final URI defaultPostLogoutRedirectURI;
	
	
	/**
	 * Creates a new logout page configuration.
	 *
	 * @param apiEndpoint                  The Connect2id server logout
	 *                                     session API endpoint.
	 * @param apiAccessToken               The bearer access token for the
	 *                                     logout session API.
	 * @param cookieName                   The name of the cookie used to
	 *                                     store the subject (end-user)
	 *                                     session ID.
	 * @param defaultPostLogoutRedirectURI Optional default post logout
	 *                                     redirection URI.
	 */
	public Configuration(final URI apiEndpoint,
			     final BearerAccessToken apiAccessToken,
			     final String cookieName,
			     final URI defaultPostLogoutRedirectURI) {
		this.apiEndpoint = apiEndpoint;
		this.apiAccessToken = apiAccessToken;
		this.cookieName = cookieName;
		this.defaultPostLogoutRedirectURI = defaultPostLogoutRedirectURI;
	}
	
	
	/**
	 * Creates a new logout page configuration from the specified
	 * properties, overridden by Java system properties with the same name.
	 *
	 * @param props The properties.
	 *
	 * @throws PropertyParseException If parsing of the properties failed.
	 */
	public Configuration(final Properties props)
		throws PropertyParseException {
		
		var pr = new PropertyRetriever(props, true);
		
		try {
			apiEndpoint = new URI(pr.getString("logoutPage.apiEndpoint"));
		} catch (URISyntaxException e) {
			throw new PropertyParseException("Invalid URI: " + e.getMessage(), "logoutPage.apiEndpoint");
		}
		
		apiAccessToken = new BearerAccessToken(pr.getString("logoutPage.apiAccessToken"));
		
		cookieName = pr.getString("logoutPage.cookieName");
		
		String uriValue = pr.getOptString("logoutPage.defaultPostLogoutRedirectURI", null);
		
		if (uriValue != null) {
			try {
				defaultPostLogoutRedirectURI = new URI(uriValue);
			} catch (URISyntaxException e) {
				throw new PropertyParseException("Invalid URI: " + e.getMessage(), "logoutPage.defaultPostLogoutRedirectURI", uriValue);
			}
		} else {
			defaultPostLogoutRedirectURI = null;
		}
	}
	
	
	/**
	 * Returns the Connect2id server logout session API endpoint.
	 *
	 * @return The Connect2id server logout session API endpoint.
	 */
	public URI getAPIEndpoint() {
		return apiEndpoint;
	}
	
	
	/**
	 * Returns the bearer access token for the logout session API.
	 *
	 * @return The bearer access token for the logout session API.
	 */
	public BearerAccessToken getAPIAccessToken() {
		return apiAccessToken;
	}
	
	
	/**
	 * Returns the name of the cookie used to store the subject (end-user)
	 * session ID.
	 *
	 * @return The cookie name.
	 */
	public String getCookieName() {
		return cookieName;
	}
	
	
	/**
	 * Returns the optional default post logout redirection URI.
	 *
	 * @return Optional default post logout redirection URI, {@code null}
	 *         if not specified.
	 */
	public URI getDefaultPostLogoutRedirectionURI() {
		return defaultPostLogoutRedirectURI;
	}
}
