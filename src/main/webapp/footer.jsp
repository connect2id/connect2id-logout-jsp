<div class="footer container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h5>
                Sample <a href="https://connect2id.com/products/server">Connect2id server</a> logout page |
                <a href="https://bitbucket.org/connect2id/connect2id-logout-jsp/">Source</a> |
                Questions? Email <a href="mailto:support@connect2id.com">support@connect2id.com</a>
            </h5>
        </div>
    </div>
</div>