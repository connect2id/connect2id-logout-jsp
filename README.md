# Connect2id Server Logout UI

Sample [Connect2id server](https://connect2id.com/products/server) logout page
implemented with Java Server Pages (JSP).

Features:

 - Simple logout form for OpenID Connect Relying Party (RP) initiated logout 
   requests.
 - Integrates with the Connect2id server via the [logout session web
   API](https://connect2id.com/products/server/docs/integration/logout-session).
 - Requires a Jakarta Servlet 6.0.0+ compatible container (e.g. Apache Tomcat 
   10.1+) and Java 17+.

Compatible with **Connect2id Server 7.0+**.

## Configuration

See `/WEB-INF/logoutPage.properties`.


## Build

Build with `mvn clean package`. 


## Questions or comments?

Email [Connect2id support](https://connect2id.com/contact).


***
Copyright (c) Connect2id Ltd., 2017 - 2023
